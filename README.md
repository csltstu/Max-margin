This directory is used to implement the max-margin metric learning for speaker recognition.

The topics are as follows:

(1) 1_max_margin.py: 
    The python recipe for max-margin metric learning.

(2) 2_ivector.sh: 
    The shell recipe for 1_max_margin.py.

(3) 3_eva_ivector.py:
    The python recipe for evaluation.

(4) 4_eva_ivector.sh:
    The shell recipe for 3_eva_ivector.py.

(5) score_sre08.sh:
    The shell recipe for EER computation.
 
