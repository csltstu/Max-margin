#!/nfs/disk/perm/linux-x86_64/bin/python
# Copyright 2015 CSLT (Author: Lantian Li and Chao Xing) 
# Apache 2.0.

import numpy as np
import theano
import theano.tensor as T
import sys, math
import time
from collections import OrderedDict

def loadDict(Path):
	temp_dict = {}
	input_file = file(Path, 'r')
	i = 0
	for line in input_file:
		line = line.strip()
		wordList = line.split(' ')
		temp_dict[wordList[0]] = np.array(wordList[1:], dtype = theano.config.floatX)
		i += 1
	return temp_dict, len(temp_dict.values()[0])

def loadPair(Path, _dict):
	spk_dict = {}
	utt_dict = {}
	input_file = file(Path, 'r')
	for line in input_file:
		line = line.strip()
		wordList = line.split(' ')
		temp_list = []
		for value in wordList[1:]:
			if _dict.has_key(value):
				temp_list.append(value)
		spk_dict[wordList[0]] = temp_list 
	for key, values in spk_dict.items():
		for value in values:
			utt_dict[value] = key
#			print value, key
#			sys.stdout.flush()
#	print utt_dict.has_key("swc5083-swbdc_SW_40854_2")		
	return spk_dict, utt_dict

def getUser(name):
	return name.split('-')[0]

def init_params(word_dim, output_dim):
	params = OrderedDict()
	params['W'] = theano.shared(np.asarray(
				np.random.normal(0, math.sqrt(6. / (word_dim + output_dim)) / 10, size = (word_dim, output_dim)),
#				np.random.normal(0, math.sqrt(6. / (word_dim + output_dim), size = (word_dim, output_dim)),
#				np.random.uniform(low = -math.sqrt(6. / (word_dim + output_dim)) / 100, high = math.sqrt(6. / (word_dim + output_dim)) / 100, size = (word_dim, output_dim)),
				dtype = theano.config.floatX
			)
		)
	params['bias'] = theano.shared(np.zeros(output_dim, dtype = theano.config.floatX))
	
	return params

def compute_cost_updates(params, train_vectors, positive_vectors, negative_vectors, margin, E_g, E_x):
	'''
		mini-batch for linear transfer
	'''
	t_train_vectors = T.dot(train_vectors, params['W']) + params['bias']
	t_positive_vectors = T.dot(positive_vectors, params['W']) + params['bias']
	t_negative_vectors = T.dot(negative_vectors, params['W']) + params['bias']
	
	t_norm_train_vectors, temp_updates = theano.scan(fn = lambda vector : T.sqrt(T.sum(vector ** 2)), sequences = [t_train_vectors])
	t_norm_positive_vectors, temp_updates = theano.scan(fn = lambda vector : T.sqrt(T.sum(vector ** 2)), sequences = [t_positive_vectors])
	t_norm_negative_vectors, temp_updates = theano.scan(fn = lambda vector : T.sqrt(T.sum(vector ** 2)), sequences = [t_negative_vectors])


	Losts = T.diagonal(T.dot(t_train_vectors, t_negative_vectors.T)) / t_norm_train_vectors / t_norm_negative_vectors - \
			  T.diagonal(T.dot(t_train_vectors, t_positive_vectors.T)) / t_norm_train_vectors / t_norm_positive_vectors + margin

	costs = T.maximum(T.zeros_like(Losts), Losts)

	cost = T.mean(costs)

	gparams = T.grad(cost, wrt = params.values())
#	gparams = [gparam / T.sqrt(T.sum(gparam ** 2)) for gparam in gparams]

	delta_Eg = [0.95 * gt_1 + 0.05 * (g ** 2) for gt_1, g in zip(E_g, gparams)]	
	delta_X = [-T.sqrt(Ex_1 + 1e-6) * g / T.sqrt(Eg_t + 1e-6) for Ex_1, g, Eg_t in zip(E_x, gparams, delta_Eg)]

	updates_Ex = [(Ex_1, 0.95 * Ex_1 + 0.05 * (x_t ** 2))	for Ex_1, x_t in zip(E_x, delta_X)]
	updates_Eg = [(eg, d_eg) for eg, d_eg in zip(E_g, delta_Eg)]

	updates = [
				(param, param + x_t)
				for param, x_t in zip(
					params.values(), delta_X
				)
			] + updates_Eg + updates_Ex

	return cost, updates

def compute_cost_updates_nonlinear(params, train_vectors, positive_vectors, negative_vectors, learning_rate, margin):
	'''
		mini-batch for non-linear transfer
		non-linear could be sigmoid or tanh
	'''
#	t_train_vectors = T.nnet.sigmoid(T.dot(train_vectors, params['W']) + params['bias'])
#	t_positive_vectors = T.nnet.sigmoid(T.dot(positive_vectors, params['W']) + params['bias'])
#	t_negative_vectors = T.nnet.sigmoid(T.dot(negative_vectors, params['W']) + params['bias'])
	
	t_train_vectors = T.tanh(T.dot(train_vectors, params['W']) + params['bias'])
	t_positive_vectors = T.tanh(T.dot(positive_vectors, params['W']) + params['bias'])
	t_negative_vectors = T.tanh(T.dot(negative_vectors, params['W']) + params['bias'])

	Losts = T.diagonal(T.dot(t_train_vectors, t_negative_vectors.T)) - T.diagonal(T.dot(t_train_vectors, t_positive_vectors.T)) + margin

	costs = T.maximum(T.zeros_like(Losts), Losts)

	cost = T.mean(costs)

	gparams = T.grad(cost, wrt = params.values())
#	gparams = [gparam / T.sqrt(T.sum(gparam ** 2)) for gparam in gparams]
	
	updates = [(param, param - learning_rate * gparam) for param, gparam in zip(params.values(), gparams)]

	return cost, updates

def sample(index, batch_size, word_dict, spk_index, utt_index, count):
	if (index + 1) > len(word_dict):
		temp_train_index = word_dict.keys()[index * batch_size : ]
	else:
		temp_train_index = word_dict.keys()[index * batch_size : (index + 1) * batch_size]

	word_keys = spk_index.keys()
	temp_pos_index = []
	temp_neg_index = []
	train_index = []
	for temp_index in temp_train_index:
		for i in range(count): 
			train_index.append(word_dict[temp_index])
			temp_name = utt_index[temp_index]
			temp_pos_index.append(word_dict[np.random.choice(spk_index[temp_name])])
			temp_negative_name = np.random.choice(word_keys)
			while (temp_negative_name == temp_name) or (len(spk_index[temp_negative_name]) == 0):
				temp_negative_name = np.random.choice(word_keys)
			temp_neg_index.append(word_dict[np.random.choice(spk_index[temp_negative_name])])
	return np.asarray(train_index, dtype = theano.config.floatX), np.asarray(temp_pos_index, dtype = theano.config.floatX), np.asarray(temp_neg_index, dtype = theano.config.floatX)
			
def normal_matrix(word_matrix):
	for i in range(word_matrix.shape[1]):
		word_matrix[:, i] = (word_matrix[:, i] - np.mean(word_matrix[:, i])) / np.cov(word_matrix[:, i])
	return word_matrix

def transfer_train(params, max_iter, batch_size, threshold, alpha, margin, spk_index, utt_index, word_dict, max_patience, count):
	train_vectors = T.fmatrix('tv')
	positive_vectors = T.fmatrix('pv')
	negative_vectors = T.fmatrix('nv')
	learning_rate = T.fscalar('lr')
	index = T.lscalar('index')
#	word_matrix = theano.shared(normal_matrix(np.asarray(word_dict.values(), dtype = theano.config.floatX)))

	E_g = [theano.shared(p.get_value() * np.asarray(0., dtype = theano.config.floatX)) for k, p in params.iteritems()]
	E_x = [theano.shared(p.get_value() * np.asarray(0., dtype = theano.config.floatX)) for k, p in params.iteritems()]

	cost, updates = compute_cost_updates(params, train_vectors = train_vectors, positive_vectors = positive_vectors, negative_vectors = negative_vectors, margin = margin, E_g = E_g, E_x = E_x)
#	cost, updates = compute_cost_updates_nonlinear(params, train_vectors = train_vectors, positive_vectors = positive_vectors, negative_vectors = negative_vectors, learning_rate = learning_rate, margin = margin)

	t_transfer_train = theano.function(
			inputs = [train_vectors, positive_vectors, negative_vectors],
			outputs = [cost],
			updates = updates,
		)
	
	'''
		train process
	'''
	iter = 0
	old_state = [float('inf'), params]
	n_batchs = len(word_dict) / batch_size
	learning_alpha = alpha
	patience = 0

	while iter != max_iter:
		c = []
		start_time = time.clock()
		for index in range(n_batchs):
			t_train_vectors, t_positive_vectors, t_negative_vectors = sample(index, batch_size, word_dict, spk_index, utt_index, count)
			temp_cost = t_transfer_train(t_train_vectors, t_positive_vectors, t_negative_vectors)
			c.append(temp_cost)
#			sys.stderr.write('.')
#			sys.stderr.write(str(temp_cost) + '\n')
		if n_batchs * batch_size < len(word_dict):
			t_train_vectors, t_positive_vectors, t_negative_vectors = sample(n_batchs, batch_size, word_dict, spk_index, utt_index, count)
			temp_cost = t_transfer_train(t_train_vectors, t_positive_vectors, t_negative_vectors)
		end_time = time.clock()
		train_cost = np.mean(c)

		sys.stderr.write('\n\tIteration : ' + str(iter) + ' cost is : ' + str(train_cost) + ' spend : ' + str(end_time - start_time) + ' sec/epoch ' + '\n')
		
		if abs(train_cost - old_state[0]) < threshold :
			break
		
		if train_cost < old_state[0]:
			old_state = [train_cost, params]
			patience = 0
		else:
			params = old_state[1]
			patience += 1
			if patience == max_patience:
			#	learning_alpha *= 2. / 3
			#	patience = 0
				break
		iter += 1
	
	return params
	
def trainModel(options):
	print options
	sys.stdout.flush()
	if options.has_key('input'):
		word_vectors_path = options['input']
	else:
		exit('Need word vectors path')
	
	if options.has_key('input_list'):
		word_name_path = options['input_list']
	else:
		exit('Need word name path')
	
	if options.has_key('output'):
		output_path = options['output']
	else:
		exit('Need output path')
		
	
	if options.has_key('max_iter'):
		max_iter = options['max_iter']
	else:
		max_iter = -1
	
	if options.has_key('threshold'):
		threshold = options['threshold']
	else:
		threshold = 1e-6
	
	if options.has_key('margin'):
		margin = options['margin']
	else:
		margin = 1
	
	if options.has_key('alpha'):
		alpha = options['alpha']
	else:
		alpha = 0.25
	
	if options.has_key('size'):
		output_size = options['size']
	else:
		output_size = 100
	
	if options.has_key('batch'):
		batch_size = options['batch']
	else:
		batch_size = 100

	if options.has_key('max_patience'):
		max_patience = options['max_patience']
	else:
		max_patience = 20

	if options.has_key('count'):
		count = options['count']
	else:
		count = 5

	sys.stderr.write('Load word vectors....\n')
	word_vectors, word_dim = loadDict(word_vectors_path)
	sys.stderr.write('Load word vectors done....\n')
	
	sys.stderr.write('Load name pairs....\n')
	spk_pair, utt_pair = loadPair(word_name_path, word_vectors)
	sys.stderr.write('Load name pairs done....\n')
	
	params = init_params(word_dim, output_size)

	params = transfer_train(params, max_iter = max_iter, threshold = threshold, batch_size = batch_size, alpha = alpha, margin = margin, spk_index = spk_pair, utt_index = utt_pair, word_dict = word_vectors, max_patience = max_patience, count = count)
	
	output_file = file(output_path + '_W', 'w')
	output_file.write(
		'\n'.join(
			[' '.join(
				list(
					np.asarray(
						W, dtype = str
					)))
				for W in params['W'].get_value()
			]
		)
	)
	output_file.close()
	output_file = file(output_path + '_bias', 'w')
	output_file.write(
		' '.join(
			list(
				np.asarray(params['bias'].get_value(), dtype = str)
			)
		)
	)

def showHelp():
	sys.stderr.write('\
		\n\t-input <file>;\
		\n\t\tSet word vectors path. Can not be omited.\
		\n\t-input-pair <file>;\
		\n\t\tSet word class pair path. Can not be omited.\
		\n\t-output <string>;\
		\n\t\tSet output path name. Can not be omited. Will generate two files, first is output_W, the other is output_bias.\
		\n\t-iter <int>;\
		\n\t\tSet max iteration count. Default is -1.\
		\n\t-threshold <float>;\
		\n\t\tSet threshold for early stopping. Default is 1e-4.\
		\n\t-margin <float>;\
		\n\t\tSet max margin count. Default is 1.\
		\n\t-alpha <float>;\
		\n\t\tSet learning rate. Default is 0.25.\
		\n\t-size <int>;\
		\n\t\tSet output vectors size. Default is 100.\
		\n\t-batch <int>;\
		\n\t\tSet mini-batch count. Default is 100.\
		\n\t-patience <int>;\
		\n\t\tSet max patience. Default is 5.\
		\n\t-count <int>;\
		\n\t\tSet negative count for training. Default is 5.\
		\n\n')

if __name__ == "__main__":
	options = {}
	if len(sys.argv) == 1:
		showHelp()
	else:
		for i in range(len(sys.argv) - 1):
			if sys.argv[i] == '-input':
				options['input'] = sys.argv[i + 1]
			elif sys.argv[i] == '-input-pair':
				options['input_list'] = sys.argv[i + 1]
			elif sys.argv[i] == '-output':
				options['output'] = sys.argv[i + 1]
			elif sys.argv[i] == '-iter':
				options['max_iter'] = int(sys.argv[i + 1])
			elif sys.argv[i] == '-threshold':
				options['threshold'] = float(sys.argv[i + 1])
			elif sys.argv[i] == '-margin':
				options['margin'] = float(sys.argv[i + 1])
			elif sys.argv[i] == '-alpha':
				options['alpha'] = float(sys.argv[i + 1])
			elif sys.argv[i] == '-size':
				options['size'] = int(sys.argv[i + 1])
			elif sys.argv[i] == '-batch':
				options['batch'] = int(sys.argv[i + 1])
			elif sys.argv[i] == '-patience':
				options['max_patience'] = int(sys.argv[i + 1])
			elif sys.argv[i] == '-count':
				options['count'] = int(sys.argv[i + 1])
		trainModel(options)
