#!/nfs/disk/perm/linux-x86_64/bin/python
# Copyright 2015 CSLT (Author: Lantian Li and Chao Xing)
# Apache 2.0.

import numpy as np
import sys,math
import os
import string
import theano.tensor as T
from math import sqrt

def loadMatrix(matrix_path):
	input_file = file(matrix_path)
	temp_list = []
	for line in input_file:
		line = line.strip()
		wordList = line.split(' ')
		temp_list.append(np.array(wordList, dtype = float))
	return temp_list

if __name__ =="__main__":
	
	fin_train = sys.argv[1]
	fin_test = sys.argv[2]
	fin_W = sys.argv[3]
        fin_bias = sys.argv[4]
	fout_dir = sys.argv[5]
	
	isExists = os.path.exists(fout_dir)
	if not isExists:
		os.makedirs(fout_dir)

	A = loadMatrix(fin_W)
	b = loadMatrix(fin_bias)
	dim = len(b[0])

	fp = open(fin_train, "r")
	fpOutTrain = open(fout_dir + os.sep + "train.ark", "w")
	lines = fp.readlines()
	for line in lines:
	    items = line.split();
	    x = np.array(items[1:], dtype = float)
	    temp = np.dot(x, A) + b
	    y = temp
#	    y = np.tanh(temp)
#	    y = T.nnet.sigmoid(y)
            outputLine = items[0]
            outputLine += "  ["
            for i in range(dim):
                outputLine += " "
                outputLine += str(y[0][i])
            outputLine += " ]\n"
            fpOutTrain.write(outputLine)
	fp.close()
	fpOutTrain.close()
	
	fp = open(fin_test, "r")
	fpOutTest = open(fout_dir + os.sep + "test.ark", "w")
	lines = fp.readlines()
	for line in lines:
	    items = line.split();
	    cnt = len(items)
	    x = np.array(items[1:], dtype = float)
	    temp = np.dot(x, A) + b
	    y = temp
#	    y = np.tanh(temp)
#	    y = T.nnet.sigmoid(y)
            outputLine = items[0]
            outputLine += "  ["
            for i in range(dim):
                outputLine += " "
                outputLine += str(y[0][i])
            outputLine += " ]\n"
            fpOutTest.write(outputLine)	
	fp.close()
	fpOutTest.close()
