#!/bin/bash
# Copyright 2015 CSLT (Author: Lantian Li and Chao Xing)
# Apache 2.0.

dir=$1
data_dir=$2
#----------------------------------------#

for lr in 0.2 ;do
       	for margin in 1 ;do
               	for dim in `seq 100 401`;do
                       	tmp=$(($dim%50))
                       	if [ $tmp -eq 0 ] ;then
				conf=_W_lr${lr}_mr${margin}_${dim}
                               	echo $conf
	
				exp_dir=$dir/${conf}
				python 3_eva_ivector.py $data_dir/spk_ivector_train.ark $data_dir/spk_ivector_test.ark $exp_dir/_W $exp_dir/_bias $exp_dir
		
				ivector-normalize-length ark:$exp_dir/train.ark ark,scp:$exp_dir/norm_train.ark,$exp_dir/norm_train.scp
				ivector-normalize-length ark:$exp_dir/test.ark ark,scp:$exp_dir/norm_test.ark,$exp_dir/norm_test.scp

	#cosine
				trials=$data_dir/short2-short3-female-total.trials
				cat $trials | awk '{print $1, $2}' | \
				ivector-compute-dot-products - \
				scp:$exp_dir/norm_train.scp \
				scp:$exp_dir/norm_test.scp \
				$exp_dir/foo_margin_

				./score_sre08.sh $trials $exp_dir/foo_margin_
			fi
		done
	done
done

