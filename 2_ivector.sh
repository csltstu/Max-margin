#!/bin/bash
# Copyright 2015 CSLT (Author: Lantian Li and Chao Xing)
# Apache 2.0.


dir=$1
datadir=$2

for lr in 0.2 ;do
	for margin in 1 ;do
		for dim in `seq 50 150`;do
			tmp=$(($dim%50))
			if [ $tmp -eq 0 ] ;then	
				exp_dir=$dir/exp-transfer-ivector/_W_lr${lr}_mr${margin}_${dim}
				mkdir -p $exp_dir 	
				THEANO_FLAGS=mode=FAST_RUN,device=gpu,floatX=float32 ./1_max_margin.py -input $datadir/ivector-norm.ark -input-pair $datadir/spk2utt -output $exp_dir/ -margin ${margin} -alpha $lr -size ${dim} -count 10 2&> $exp_dir/_W_lr${lr}_mr${margin}_${dim}.log
			fi
		done
	done
done
echo Successed!
